plugins {
  java
  id("idea")
  id("org.springframework.boot") version "3.1.4"
  id("io.spring.dependency-management") version "1.1.3"
}

group = "dev.fresult"
version = "0.0.1-SNAPSHOT"

java {
  sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

repositories {
  mavenCentral()
}

dependencies {
  compileOnly("org.projectlombok:lombok")

//  implementation("com.gradle.build-scan:com.gradle.build-scan.gradle.plugin:3.13")
  implementation("org.springframework.boot:spring-boot-starter-web")
  implementation("org.springframework.boot:spring-boot-starter-log4j2")
  implementation("org.springframework.boot:spring-boot-starter-data-jpa")
  implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml:2.15.2")
  implementation("com.lmax:disruptor:3.4.4")
  implementation("org.springframework.boot:spring-boot-starter-undertow")
  implementation("org.jetbrains.kotlin:kotlin-reflect")

  runtimeOnly("com.h2database:h2")
  developmentOnly("org.springframework.boot:spring-boot-devtools")
  annotationProcessor("org.projectlombok:lombok")

  testImplementation("org.springframework.boot:spring-boot-starter-test")
}

configurations {
  named("implementation") {
    exclude(group = "org.springframework.boot", module = "spring-boot-starter-tomcat")
    exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}
