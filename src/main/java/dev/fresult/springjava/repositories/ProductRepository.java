package dev.fresult.springjava.repositories;

import dev.fresult.springjava.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Long> {

//  @Modifying(flushAutomatically = true, clearAutomatically = true)
//  @Query(value = "UPDATE dev.fResult.springjava.entities.Product p set p.name = ?1 WHERE p.code = ?2")
//  int jpqlUpdate(String name,String code);
//
//  @Modifying(flushAutomatically = true, clearAutomatically = true)
//  @Query(value = "UPDATE products set name = ?1 WHERE code = ?2", nativeQuery = true)
//  int sqlUpdate(String name,String code);

}
