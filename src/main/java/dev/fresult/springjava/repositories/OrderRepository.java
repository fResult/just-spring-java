package dev.fresult.springjava.repositories;

import dev.fresult.springjava.entities.Order;
import dev.fresult.springjava.entities.embedded.OrderId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface OrderRepository extends JpaRepository<Order, OrderId> {
}
