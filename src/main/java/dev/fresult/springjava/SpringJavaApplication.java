package dev.fresult.springjava;

import dev.fresult.springjava.entities.Order;
import dev.fresult.springjava.entities.embedded.OrderId;
import dev.fresult.springjava.entities.Product;
import dev.fresult.springjava.repositories.OrderRepository;
import dev.fresult.springjava.repositories.ProductRepository;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

@Log4j2
@SpringBootApplication
public class SpringJavaApplication implements CommandLineRunner {
  @Autowired
  ProductRepository productRepo;

   @Autowired
   private OrderRepository orderRepo;

  public static void main(String[] args) {
    val INDENT_TAB = "\t\t\t\t\t";

    ConfigurableApplicationContext app = SpringApplication.run(SpringJavaApplication.class, args);
    ConfigurableEnvironment env = app.getEnvironment();

    log.info("SpringJavaApplication is running on:");
    log.info(INDENT_TAB + "Port: {}", env.getProperty("server.port"));
    log.info(INDENT_TAB + "Context Path: {}", env.getProperty("server.servlet.context-path"));
  }

  @Override
  public void run(String... args) {
    Product product = new Product();
    product.setCode("P001");
    product.setName("Coffee");
    product.setStatus(Product.Status.APPROVED);

    product.getAliasNames().add("Java");
    product.getAliasNames().add("Cuppa");
    product.getAliasNames().add("Caffeine");
    product = productRepo.save(product);

    OrderId orderId = new OrderId();
    orderId.setId(1L);
    orderId.setProductId(product.getId());

    Order order = new Order();
    order.setOrderId(orderId);
    order.setQuantity(30);
    order = orderRepo.save(order);

    log.debug(product);
    log.debug(order);

  }
}
