package dev.fresult.springjava.entities;

import dev.fresult.springjava.entities.embedded.OrderId;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Nationalized;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "orders")
/* @DynamicInsert and @DynamicUpdate are used for reduce SQL INSERT and UPDATE only fields which update
 * - Pro - reduce the SQL code, make query performance better
 * - Con - It will not use SQL in the cached (if not use @DynamicInsert and/or @DynamicUpdate, it will use cached)
 */
@DynamicInsert
@DynamicUpdate
public class Order extends CommonEntity {
  /* idClass
   * Pro: Able to use GeneratedId()
   * Con: Declare many attributes
   */
  /* @Id */
  /* private long id; */

  /* @Id */
  /* private long productId; */

  /* EmbeddedId
   * Pro: Declare less attributes
   * Con: Cannot use GeneratedId(). So, we need to manual
   */
  @EmbeddedId
  @AttributeOverrides(value = {
      @AttributeOverride(name = "id", column = @Column(name = "id")),
      @AttributeOverride(name = "productId", column = @Column(name = "pd_id"))
  })
  private OrderId orderId;

  private int quantity;

  // CLOB - Use `@Lob`
  @Lob
  private String details;

  // NCLOB - Use `@Lob` & `@Nationalized`
  @Lob
  @Nationalized
  private String nationalDetails;

  // BLOB - Use `@Lob` & `byte[]` type
  @Lob
  private byte[] photos;

}
