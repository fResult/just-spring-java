package dev.fresult.springjava.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "products")
@Table(indexes = {
    @Index(name = "products_idx_code_uniq", columnList = "code", unique = true),
    @Index(name = "products_idx_status", columnList = "status")
})
@SequenceGenerator(name = "products_seq")
public class Product extends CommonEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_seq")
  private Long id;
  private String code;
  private String name;
  private String detail;

  @Column(name = "alias_name", length = 20)
  @ElementCollection
  @CollectionTable(name = "products_alias_names", joinColumns = @JoinColumn(name = "product_id"))
  private Set<String> aliasNames = new HashSet<>();

  // @Enumerated(EnumType.STRING)
  // @Convert(converter = ProductStatusConverter.class)
  @Column(length = 1)
  private Status status;

  // 3 Tips: field for flagging or calculate something
  // @Transient private String excludedField1;
  // private transient String excludedField2;
  // private static String excludedField3;

  @Getter
  @RequiredArgsConstructor
  public enum Status {
    APPROVED("A"),
    NOT_APPROVED("N"),
    PENDING("P");

    private final String code;

    public static Status codeToStatus(String code) {
      return Stream.of(Status.values()).parallel()
          .filter(status -> status.getCode().equalsIgnoreCase(code))
          .findAny().orElseThrow(() -> new IllegalArgumentException("The code: " + code + " is not existed."));
    }
  }
}
