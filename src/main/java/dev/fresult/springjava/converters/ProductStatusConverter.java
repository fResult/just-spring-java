package dev.fresult.springjava.converters;

import dev.fresult.springjava.entities.Product;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.Objects;

/* Converter has limitation, that cannot convert...
 * - `id` fields
 * - Relationship fields
 * - @Temporal and @Enumerated fields
 */
@Converter(autoApply = true)
public class ProductStatusConverter implements AttributeConverter<Product.Status, String> {

  /**
   * @param status the entity attribute value to be converted
   * @return Product's status code
   */
  @Override
  public String convertToDatabaseColumn(Product.Status status) {
    return Objects.isNull(status) ? null : status.getCode();
  }

  /**
   * @param code the data from the database column to be
   *               converted
   * @return Product's status
   */
  @Override
  public Product.Status convertToEntityAttribute(String code) {
    return Objects.isNull(code) ? null : Product.Status.codeToStatus(code);
  }
}
