package dev.fresult.springjava.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {
  @GetMapping
  public String all() {
    System.out.println("TEST");
    return "Hello List";
  }

  @GetMapping("/{id}")
  public String byId(@PathVariable Long id) {
    return "Hello, products by ID: " + id;
  }

}
